#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>

clock_t startm, stopm;
#define START if ( (startm = clock()) == -1) {fprintf(stderr, "Error calling clock");exit(1);}
#define STOP if ( (stopm = clock()) == -1) {fprintf(stderr, "Error calling clock");exit(1);}
#define PRINTTIME fprintf(stderr, "%.3f seconds used by the processor.\n", ((double)(stopm-startm) * 1000)/CLOCKS_PER_SEC);


/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

int G_NUMBER_OF_POSSIBLE_ACTIONS = 0;
int G_NUTRIENTS = 0;
int G_NUMBER_OF_CELLS = 0;
int G_NUMBER_OF_TREES = 0;
int G_NUMBER_OF_MYTREES = 0;
int G_DAY = 0;
int G_SCORE_MIN = -37;
int G_FORBID = -38;
int G_PRIORITY = 5;
int** G_CELLS_INFO; //Array[index] ([cell_index][richness][temp my tree size][neighbours])
int** G_TREES;  //Array[index] ([cell_index][size][is_dormant])


int print_cells_info()
{
    fprintf(stderr, "__G_CELLS_INFO__\n");
    for (int i = 0; i < G_NUMBER_OF_CELLS; i++)
    {
        for (int j = 0; j < 9; j++)
            fprintf(stderr, "[%i]", G_CELLS_INFO[i][j]);
        fprintf(stderr, "\n");
    }

    return (0);
}

int** create_mat(int** mat, int l, int c)
{
    mat = malloc(sizeof(*mat) * l);
    for (int i = 0; i < l; i++)
    {
        mat[i] = malloc(sizeof(**mat) * c);
    }
    return mat;
}

int otoi(char* s)
{
    int i = 0;
    if (strcmp(s, "GROW") == 0)
        i = 1;
    if (strcmp(s, "SEED") == 0)
        i = 2;
    if (strcmp(s, "COMPLETE") == 0)
        i = 3;
    return (i);
}

char* itoo(int i)
{
    if (i == 0)
        return ("WAIT");
    if (i == 1)
        return ("GROW");
    if (i == 2)
        return ("SEED");
    if (i == 3)
        return ("COMPLETE");
}

int** parse_action(int i, char* string, int** tab)
{
    //fprintf(stderr, "Inside parse_action\n");
    //fprintf(stderr, "string = %s\n", string);
    char* ptr = strtok(string, " ");
    //fprintf(stderr, "ptr = %s\n", ptr);
    tab[i][0] = otoi(ptr);
    for (int j = 1; j < 3; j++)
    {
        ptr = strtok(NULL, " ");
        //fprintf(stderr, "ptr = %s\n", ptr);
        if (ptr != NULL)
            tab[i][j] = atoi(ptr);
        else
            tab[i][j] = 0;
    }
    return(tab);
}

int have_seed(int** G_TREES)
{
    bool res = 0;
    for (int i = 0; i < G_NUMBER_OF_TREES; i++)
    {
        if (G_TREES[i][1] == 0 && G_TREES[i][2] == 1)
        {
            res = 1;
            break;
        }
    }
    return (res);
}

int count_size3()
{
    int count = 0;

    for (int i = 0; i < G_NUMBER_OF_TREES; i++)
    {
        if (G_TREES[i][1] == 3 && G_TREES[i][2] == 1)
            count++;
    }
    //fprintf(stderr, "I have %i size3\n", count);
    return (count);

}

bool do_i_complete()
{
    bool res = 1;
    if (G_DAY < 12)
        res = 0;
    else if (count_size3() < 4 && G_DAY < 22)
        res = 0;
    return (res);
}

int seed_neighbours_bonus(int cell_index)
{
    //fprintf(stderr, "Inside neigh bonus\n");
    int bonus = 12;

    for (int i = 3; i < 9; i++)
    {
        //        fprintf(stderr, "Inside for\n");
        if (G_CELLS_INFO[cell_index][i] > -1 && G_CELLS_INFO[G_CELLS_INFO[cell_index][i]][2] > -1)//size of neigh index neigh index is cellinfo(target)(i)
        {
            fprintf(stderr, "cellindex = %i, neigh target = %i, size = %i\n", cell_index, G_CELLS_INFO[cell_index][i], G_CELLS_INFO[G_CELLS_INFO[cell_index][i]][1]);
            bonus -= 2;
        }
    }
    //fprintf(stderr, "Bonus = %i\n", bonus);
    return (bonus);
}

int** eval_action(int** action)
{
    fprintf(stderr, "Inside eval_action\n");
    for (int i = 0; i < G_NUMBER_OF_POSSIBLE_ACTIONS; i++)
    {
        //fprintf(stderr, "action = %s %i %i\n", itoo(action[i][0]), action[i][1], action[i][2]);
        switch (action[i][0])
        {
        case 0: //WAIT
            action[i][3] = 0 + G_SCORE_MIN;
            break;
        case 1: //GROW
            action[i][3] = G_CELLS_INFO[action[i][1]][1] + G_CELLS_INFO[action[i][1]][2] + G_SCORE_MIN;
            action[i][3] += (G_CELLS_INFO[action[i][1]][2] == 2 && G_DAY < 23) ? G_PRIORITY : 0;
            action[i][3] += (G_CELLS_INFO[action[i][1]][2] == 1 && G_DAY < 6) ? G_PRIORITY * 2 : 0;
            //                fprintf(stderr, "Richness =%i, size = %i\n", G_CELLS_INFO[action[i][1]][1], G_CELLS_INFO[action[i][1]][2]);
            break;
        case 2: //SEED
            if (G_DAY > 1 && G_DAY < 15 && have_seed(G_TREES) == 0) //DAY pour ne plus SEED au pif total
                action[i][3] = G_CELLS_INFO[action[i][2]][1] + G_SCORE_MIN + seed_neighbours_bonus(action[i][2]);
            else
                action[i][3] = G_FORBID;
            break;
        case 3: //COMPLETE
            if (do_i_complete())
                action[i][3] = G_NUTRIENTS + G_CELLS_INFO[action[i][1]][1] + G_SCORE_MIN;
            else
                action[i][3] = G_FORBID;
            break;
        }
        //fprintf(stderr, "score = %i\n", action[i][3]);
    }
    return (action);
}

int chose_action(int** action)
{
    int score = G_SCORE_MIN;
    int index = 0;
    eval_action(action);
    for (int i = 0; i < G_NUMBER_OF_POSSIBLE_ACTIONS; i++)
    {
        if (action[i][3] > score)
        {
            score = action[i][3];
            index = i;
        }
    }
    printf("%s %i %i %s %i %i\n", itoo(action[index][0]), action[index][1], action[index][2], itoo(action[index][0]), action[index][1], action[index][2]);
    return (0);
}

int chose_smolindex_action(int** action)
{
    //action = eval_action(action);
    int cell_index = 100;
    int tab_index = 0;
    for (int i = 0; i < G_NUMBER_OF_POSSIBLE_ACTIONS; i++)
    {
        if ((action[i][1] < cell_index) && (action[i][1] != 0))
        {
            cell_index = action[i][1];
            tab_index = i;
        }
    }
    if (cell_index == 100)
    {
        cell_index = 0;
        tab_index = 0;
    }
    //fprintf(stderr, "First action = %s\n", itoo(action[0][0]));
    //fprintf(stderr, "Last action = %s\n", itoo(action[G_NUMBER_OF_POSSIBLE_ACTIONS - 1][0]));
    printf("%s %i %i\n", itoo(action[tab_index][0]), cell_index, action[tab_index][2]);
    return (0);
}

int** reset_cells_info_size()
{
    for (int i = 0; i < G_NUMBER_OF_CELLS; i++)
    {
        G_CELLS_INFO[i][2] = -1;
    }
    return (G_CELLS_INFO);
}

int main()
{
    // 37

    scanf("%d", &G_NUMBER_OF_CELLS);
    G_CELLS_INFO = create_mat(G_CELLS_INFO, G_NUMBER_OF_CELLS, 9);
    for (int i = 0; i < G_NUMBER_OF_CELLS; i++) {
        // 0 is the center cell, the next cells spiral outwards
        int index;
        // 0 if the cell is unusable, 1-3 for usable cells
        int richness;
        // the index of the neighbouring cell for each direction
        //there is an empty case for the size rempli later
        int neigh_0;
        int neigh_1;
        int neigh_2;
        int neigh_3;
        int neigh_4;
        int neigh_5;
        scanf("%d%d%d%d%d%d%d%d", &G_CELLS_INFO[i][0], &G_CELLS_INFO[i][1], &G_CELLS_INFO[i][3], &G_CELLS_INFO[i][4], &G_CELLS_INFO[i][5], &G_CELLS_INFO[i][6], &G_CELLS_INFO[i][7], &G_CELLS_INFO[i][8]);
    }

    //print_cells_info();
    // game loop
    int tour = 0;
    while (1) {
        fprintf(stderr, "Inside while(1)\n");
        START;
        // the game lasts 24 days: 0-23
        int day;

        scanf("%d", &G_DAY);
        // the base score you gain from the next COMPLETE action
        scanf("%d", &G_NUTRIENTS);
        // your sun points
        int sun;
        // your current score
        int score;
        scanf("%d%d", &sun, &score);
        // opponent's sun points
        int opp_sun;
        // opponent's score
        int opp_score;
        // whether your opponent is asleep until the next day
        bool opp_is_waiting;
        int _opp_is_waiting;
        scanf("%d%d%d", &opp_sun, &opp_score, &_opp_is_waiting);
        opp_is_waiting = _opp_is_waiting;
        // the current amount of trees
        scanf("%d", &G_NUMBER_OF_TREES);
        fprintf(stderr, "CReating G_TREES\n");
        G_CELLS_INFO = reset_cells_info_size();
        //print_cells_info();
        G_TREES = create_mat(G_TREES, G_NUMBER_OF_TREES, 4);
        for (int i = 0; i < G_NUMBER_OF_TREES; i++) {
            // location of this tree
            int cell_index;
            // size of this tree: 0-3
            int size;
            // 1 if this is your tree
            bool is_mine;
            // 1 if this tree is dormant
            bool is_dormant;
            int _is_mine;
            int _is_dormant;
            scanf("%d%d%d%d", &cell_index, &size, &_is_mine, &_is_dormant);
            is_mine = _is_mine;
            is_dormant = _is_dormant;
            G_CELLS_INFO[cell_index][2] = size;
            if (is_mine == 1)
            {
                G_TREES[i][0] = cell_index;
                G_TREES[i][1] = size;
                G_TREES[i][2] = is_mine;
                G_TREES[i][3] = is_dormant;

            }
        }
        //print_cells_info();
        // all legal actions
        //fprintf(stderr, "G_TREES created. Creating legal actions\n");
        int number_of_possible_actions;
        scanf("%d", &G_NUMBER_OF_POSSIBLE_ACTIONS); fgetc(stdin);
        int** action;
        action = create_mat(action, G_NUMBER_OF_POSSIBLE_ACTIONS, 4);
        for (int i = 0; i < G_NUMBER_OF_POSSIBLE_ACTIONS; i++) {
            // try printing something from here to start with
            char possible_action[32];
            scanf("%[^\n]", possible_action); fgetc(stdin);
            action = parse_action(i, possible_action, action);
        }
        //fprintf(stderr, "Legal actions created\n");
        // Write an action using printf(). DON'T FORGET THE TRAILING \n
        // To debug: fprintf(stderr, "Debug messages...\n");


        // GROW cellIdx | SEED sourceIdx targetIdx | COMPLETE cellIdx | WAIT <message>

        //fprintf(stderr, "Before chose_action\n");
        STOP;
        PRINTTIME;
        //fprintf(stderr, "%ld", *start_time - time(NULL));
       // chose_action(action);

        chose_action(action);
        //chose_smolindex_action(action);
        tour++;

        //Dont forget to free G_TREES
    }

    return 0;
}